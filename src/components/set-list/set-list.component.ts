import { Component, Input } from '@angular/core';

@Component({
  selector: 'set-list',
  templateUrl: 'set-list.component.html'
})
export class SetListComponent {
    @Input() index: number;
    @Input() exercise: any;

  constructor() {}

}
