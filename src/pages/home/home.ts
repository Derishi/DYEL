import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public workoutPlan: any = [
    {
      exerciseName: "Barbell Press",
      sets: [
        {
          repGoal: 8,
          weight: 135
        },
        {
          repGoal: 8,
          weight: 135
        },
        {
          repGoal: 8,
          weight: 135
        }
      ]
    },
    {
      exerciseName: "Barbell Squat",
      sets: [
        {
          repGoal: 5,
          weight: 225
        },
        {
          repGoal: 5,
          weight: 225
        },
        {
          repGoal: 5,
          weight: 225
        },
        {
          repGoal: 5,
          weight: 225
        },
        {
          repGoal: 5,
          weight: 225
        }
      ]
    },
  ];

  constructor(public navCtrl: NavController) {

  }

}
